webpackJsonp([42],{

/***/ 642:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivePageModule", function() { return LivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__live__ = __webpack_require__(761);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var LivePageModule = /** @class */ (function () {
    function LivePageModule() {
    }
    LivePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__live__["a" /* LivePage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__node_modules_ion_bottom_drawer__["b" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], LivePageModule);
    return LivePageModule;
}());

//# sourceMappingURL=live.module.js.map

/***/ }),

/***/ 761:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash__ = __webpack_require__(447);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_Subscription__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_Subscription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_Subscription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__live_single_device_live_single_device__ = __webpack_require__(430);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_geocoder_geocoder__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_socket_connection_socket_connection__ = __webpack_require__(57);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import * as io from 'socket.io-client';









var LivePage = /** @class */ (function () {
    function LivePage(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, socialSharing, alertCtrl, modalCtrl, plt, viewCtrl, translate, toastCtrl, geocoderApi, socketConn) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.socialSharing = socialSharing;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.plt = plt;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        this.socketConn = socketConn;
        // countFlag: number = 0;
        this.navigateButtonColor = '#012B72'; //Default Color
        this.navColor = '#fff';
        this.policeButtonColor = '#012B72';
        this.policeColor = '#fff';
        this.petrolButtonColor = "#012B72";
        this.petrolColor = '#fff';
        this.shouldBounce = true;
        this.dockedHeight = 80;
        this.distanceTop = 200;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 50;
        this.transition = '0.85s ease-in-out';
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.data = {};
        // socketSwitch: any = {};
        this.socketChnl = [];
        this.socketData = {};
        this.allData = {};
        this.titleText = 'multiple';
        this.isEnabled = false;
        this.showMenuBtn = false;
        this.mapHideTraffic = false;
        this.mapData = [];
        this.geodata = [];
        this.geoShape = [];
        this.locations = [];
        this.shwBckBtn = false;
        this.deviceDeatils = {};
        this.showaddpoibtn = false;
        this.zoomLevel = 18;
        this.tempMarkArray = [];
        this.nearbyPolicesArray = [];
        this.nearbyPetrolArray = [];
        this.socketurl = "https://www.oneqlik.in";
        this.displayNames = true;
        this.icons = {};
        this.hideMe = false;
        this.resumeListener = new __WEBPACK_IMPORTED_MODULE_8_rxjs_Subscription__["Subscription"]();
        this.expectation = {};
        this.polyLines = [];
        this.marksArray = [];
        this.markersArray = [];
        if (localStorage.getItem('Total_Vech') !== null) {
            this.t_veicle_count = JSON.parse(localStorage.getItem('Total_Vech'));
        }
        this.callBaseURL();
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == this.translate.instant('Hybrid')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == this.translate.instant('Normal')) {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == this.translate.instant('Terrain')) {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == this.translate.instant('Satellite')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        this.menuActive = false;
        if (localStorage.getItem('DisplayVehicleName') !== null) {
            if (localStorage.getItem('DisplayVehicleName') == 'OFF') {
                this.displayNames = false;
            }
        }
    }
    LivePage.prototype.ionViewWillLeave = function () {
        var _this = this;
        console.log("multiple screen ionViewWillLeave");
        this.plt.ready().then(function () {
            _this.resumeListener.unsubscribe();
        });
        // this.socketConn.gpsRemoveAllListners(this.socketChnl);
        // this.socketConn.userACCSocketDisconnect();
    };
    LivePage.prototype.ionViewDidLeave = function () {
        var _this = this;
        this.plt.ready().then(function () {
            _this.resumeListener.unsubscribe();
        });
        clearInterval(this._liveInterval);
    };
    LivePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        // this.initiateMap(); 
        if (this.plt.is('ios')) {
            this.shwBckBtn = true;
            this.viewCtrl.showBackButton(false);
        }
        this.plt.ready().then(function () {
            // this.socketFunct();
            _this.resumeListener = _this.plt.resume.subscribe(function () {
                var today, Christmas;
                today = new Date();
                Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
                var diffMs = (today - Christmas); // milliseconds between now & Christmas
                var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                if (diffMins >= 5) {
                    localStorage.removeItem("backgroundModeTime");
                    _this.alertCtrl.create({
                        message: _this.translate.instant("Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?"),
                        buttons: [
                            {
                                text: _this.translate.instant('YES PROCEED'),
                                handler: function () {
                                    _this.refreshMe();
                                }
                            },
                            {
                                text: _this.translate.instant('Back'),
                                handler: function () {
                                    _this.navCtrl.setRoot('DashboardPage');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    };
    LivePage.prototype.ionViewDidEnter = function () {
        console.log("multiple live ionViewDidEnter");
        this.customFunc();
    };
    //  socketFunct() {
    //   if (this.socketConn._userACC == undefined || this.socketConn._userACC.disconnected) {
    //     this.socketConn.userACCSocketConnect();
    //   }
    //   // if (this.socketConn.leavePromise != undefined) {
    //   //   await this.socketConn.leavePromise;
    //   // }
    // }
    LivePage.prototype.customFunc = function () {
        var _this = this;
        // debugger
        this.navBar.backButtonClick = function (ev) {
            _this.hideMe = true;
            _this.navCtrl.pop({
                animate: true, animation: 'transition-ios', direction: 'back'
            });
        };
        // debugger
        // if (localStorage.getItem("navigationFrom") === null) {
        this.initiateMap(); // at first we will load the map..
        // } else {
        //   this.callFunction();
        // }
        this.allData.markersArray = [];
        if (localStorage.getItem("CLICKED_ALL") != null) {
            this.showMenuBtn = true;
        }
        if (localStorage.getItem("SCREEN") != null) {
            if (localStorage.getItem("SCREEN") === 'live') {
                this.showMenuBtn = true;
            }
        }
        this.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Bottom;
        this.onClickShow = false;
        this.showBtn = false;
        this.SelectVehicle = "Select Vehicle";
        this.selectedVehicle = undefined;
    };
    LivePage.prototype.ngOnInit = function () { };
    LivePage.prototype.ngOnDestroy = function () {
        localStorage.removeItem("CLICKED_ALL");
    };
    LivePage.prototype.callFunction = function () {
        var _this = this;
        var that = this;
        this.to = new Date().toISOString();
        var d = new Date();
        var a = d.setHours(0, 0, 0, 0);
        this.from = new Date(a).toISOString();
        this.userDevices();
        this._liveInterval = setInterval(function () {
            // if (that.allData.markersArray.length > 0) {
            //   that.allData.markersArray.forEach(element => {
            //     element.remove();
            //   });
            // }
            _this.userDevices();
        }, 30000);
    };
    LivePage.prototype.newMap = function () {
        var mapOptions = {
            controls: {
                compass: false,
                zoom: false,
                myLocation: true,
                myLocationButton: false,
            },
            mapTypeControlOptions: {
                mapTypeIds: [__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].ROADMAP, 'map_style']
            },
            gestures: {
                rotate: false,
                tilt: false
            },
            mapType: this.mapKey
        };
        var map = __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
        if (this.plt.is('android')) {
            map.setPadding(20, 20, 20, 20);
        }
        return map;
    };
    LivePage.prototype.goBack = function () {
        this.navCtrl.pop({ animate: true, direction: 'forward' });
    };
    LivePage.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    LivePage.prototype.refreshMe = function () {
        this.ngOnDestroy();
        this.ionViewDidEnter();
    };
    LivePage.prototype.refreshMe1 = function () {
        this.ngOnDestroy();
        this.socketConn.socketDisconnect();
        this.ionViewDidEnter();
        // this.socketConn.gpsSocketConnect();
        // let paramData = this.data;
        // this.socketInit(paramData);
    };
    LivePage.prototype.callBaseURL = function () {
        var _this = this;
        if (localStorage.getItem("ENTERED_BASE_URL") === null) {
            var url = "https://www.oneqlik.in/pullData/getUrlnew";
            this.apiCall.getSOSReportAPI(url)
                .subscribe(function (data) {
                if (data.url) {
                    localStorage.setItem("BASE_URL", JSON.stringify(data.url));
                }
                if (data.socket) {
                    localStorage.setItem("SOCKET_URL", JSON.stringify(data.socket));
                }
                _this.getSocketUrl();
            });
        }
    };
    LivePage.prototype.getSocketUrl = function () {
        if (localStorage.getItem('SOCKET_URL') !== null) {
            this.socketurl = JSON.parse(localStorage.getItem('SOCKET_URL'));
        }
    };
    LivePage.prototype.resetMapContainer = function (div, visible) {
        var _this = this;
        setTimeout(function () {
            debugger;
            if (_this.allData.map) {
                _this.allData.map.setDiv(div);
                _this.allData.map.setVisible(visible);
            }
        }, 600); // timeout is a bit of a hack but it helps here
    };
    LivePage.prototype.initiateMap = function () {
        // if (!this.socketConn.initialMapLoad) {
        //   // subsequent loads...
        //   this.resetMapContainer('map_canvas',true); // assumes div has id of map
        // } else {
        //   // first load...
        //   this.socketConn.initialMapLoad = false;
        //   // this.allData.map.remove();
        //   this.allData.map = this.newMap();
        // }
        if (this.allData.map) {
            this.allData.map.remove();
            this.allData.map = this.newMap();
        }
        else {
            this.allData.map = this.newMap();
        }
        this.callFunction();
        if (localStorage.getItem('TrafficMode') !== null) {
            if (localStorage.getItem('TrafficMode') === 'ON') {
                this.mapHideTraffic = true;
                this.allData.map.setTrafficEnabled(true);
            }
        }
    };
    LivePage.prototype.userDevices = function () {
        var baseURLp;
        var that = this;
        baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.userdetails._id + '&email=' + this.userdetails.email;
        if (this.userdetails.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.userdetails._id;
            this.isSuperAdmin = true;
        }
        else {
            if (this.userdetails.isDealer == true) {
                baseURLp += '&dealer=' + this.userdetails._id;
                this.isDealer = true;
            }
        }
        that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (resp) {
            that.portstemp = resp.devices;
            that.mapData = [];
            that.mapData = resp.devices.map(function (d) {
                if (d.last_location !== undefined) {
                    return { lat: d.last_location['lat'], lng: d.last_location['long'] };
                }
            });
            var dummyData;
            dummyData = resp.devices.map(function (d) {
                if (d.last_location === undefined)
                    return;
                else {
                    return {
                        "position": {
                            "lat": d.last_location['lat'],
                            "lng": d.last_location['long']
                        },
                        "name": d.Device_Name,
                        "icon": {
                            "url": that.getIconUrl(d),
                            "size": {
                                "width": 20,
                                "height": 40
                            }
                        },
                    };
                }
            });
            dummyData = dummyData.filter(function (element) {
                return element !== undefined;
            });
            if (that.allData.markersArray.length == 0) {
                var bounds = new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapData);
                that.allData.map.moveCamera({
                    target: bounds,
                    zoom: 10
                });
            }
            if (that.isSuperAdmin || that.isDealer) {
                that.addCluster(dummyData);
            }
            else {
                // that.socketChnl = [];
                that.placeMarkers(resp.devices);
                // let tempArray = [];
                // for (let i = 0; i < resp.devices.length; i++) {
                //   console.log(resp.devices[i].status)
                //   if (resp.devices[i].status != "OUT OF REACH" && resp.devices[i].status != "NO DATA" && resp.devices[i].status != "Expired") {
                //     tempArray.push(resp.devices[i].Device_ID);
                //   }
                // }
                // console.log("tempArray length: ", tempArray.length);
                // that.socketConn._gps.emit('acc', tempArray);
                // setTimeout(() => {
                // for (let i = 0; i < resp.devices.length; i++) {
                //   if (resp.devices[i].status != "NO DATA" && resp.devices[i].status != "Expired") {
                //     that.socketInit(resp.devices[i]);
                //   }
                // }
                // resp.devices.forEach(element => {
                //   if (element.status != "out of reach" && element.status != "NO DATA" && element.status != "Expired") {
                //     that.socketInit(element);
                //   }
                // });
                // }, 1000)
            }
        }, function (err) {
            console.log(err);
        });
    };
    // markersKeyArray: any = [];
    LivePage.prototype.placeMarkers = function (devices) {
        var that = this;
        // debugger
        // if (that.allData.markersKeyArray.length >) {
        // }
        // that.allData.markersKeyArray = [];
        devices.forEach(function (item) {
            if (item.status != "NO DATA" && item.status != "Expired") {
                if (that.displayNames) {
                    that.placeMergedMarkers(item);
                }
                else {
                    that.allData[item._id] = {};
                    that.allData[item._id].poly = [];
                    that.allData[item._id].poly.push({ lat: item.last_location.lat, lng: item.last_location.long });
                    that.allData.map.addMarker({
                        position: { lat: item.last_location.lat, lng: item.last_location.long },
                        icon: {
                            "url": that.getIconUrl(item),
                            "size": {
                                "width": 20,
                                "height": 40
                            }
                        },
                        rotation: Number(item.heading)
                    }).then(function (mark) {
                        that.allData.markersArray.push(mark);
                        that.allData[item._id].mark = mark;
                        // if (item.status != "out of reach") {
                        //   that.socketInit(item);
                        // }
                    });
                }
            }
        });
        // alldevices.forEach(element => {
        //   if (element.status != "out of reach" && element.status != "NO DATA" && element.status != "Expired") {
        //     that.socketInit(element);
        //   }
        // });
        // console.log("check markers array: ", that.markersKeyArray);
    };
    LivePage.prototype.placeMergedMarkers = function (data) {
        var that = this;
        var c = document.createElement("canvas");
        c.width = 100;
        c.height = 60;
        var ctx = c.getContext("2d");
        ctx.fillStyle = "#1c2f66";
        ctx.fillRect(0, 0, 100, 20);
        // ctx.strokeRect(0, 0, 100, 70);
        ctx.font = "9pt sans-serif";
        ctx.fillStyle = "white";
        ctx.fillText(data.Device_Name, 12, 15);
        var img = c.toDataURL();
        if (that.allData[data._id] != undefined) {
            if (that.allData[data._id].mark1 != undefined && that.allData[data._id].mark != undefined) {
                that.allData[data._id].mark1.setPosition(new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](data.last_location.lat, data.last_location.long));
                that.allData[data._id].mark.setIcon(that.getIconUrl(data));
                that.allData[data._id].mark.setPosition(new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](data.last_location.lat, data.last_location.long));
                that.allData[data._id].mark.setRotation(Number(data.heading));
            }
            // that.allData[data._id].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
            that.allData.map.addMarker({
                position: { lat: data.last_location.lat, lng: data.last_location.long },
                icon: {
                    url: img,
                    size: {
                        height: 60,
                        width: 100
                    }
                }
            }).then(function (marker1) {
                that.allData.markersArray.push(marker1);
                that.allData[data._id].mark1 = marker1;
                that.allData.map.addMarker({
                    position: { lat: data.last_location.lat, lng: data.last_location.long },
                    icon: {
                        "url": that.getIconUrl(data),
                        "size": {
                            "width": 20,
                            "height": 40
                        }
                    },
                    rotation: Number(data.heading)
                }).then(function (marker) {
                    that.allData.markersArray.push(marker);
                    that.allData[data._id].mark = marker;
                    // if (data.status != "out of reach") {
                    //   that.socketInit(data);
                    // }
                });
            });
        }
        else {
            that.allData[data._id] = {};
            // that.allData[data._id].poly = [];
            // that.allData[data._id].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
            that.allData.map.addMarker({
                position: { lat: data.last_location.lat, lng: data.last_location.long },
                icon: {
                    url: img,
                    size: {
                        height: 60,
                        width: 100
                    }
                }
            }).then(function (marker1) {
                that.allData.markersArray.push(marker1);
                that.allData[data._id].mark1 = marker1;
                that.allData.map.addMarker({
                    position: { lat: data.last_location.lat, lng: data.last_location.long },
                    icon: {
                        "url": that.getIconUrl(data),
                        "size": {
                            "width": 20,
                            "height": 40
                        }
                    },
                    rotation: Number(data.heading)
                }).then(function (marker) {
                    that.allData.markersArray.push(marker);
                    that.allData[data._id].mark = marker;
                    // if (data.status != "out of reach") {
                    //   that.socketInit(data);
                    // }
                });
            });
        }
    };
    LivePage.prototype.addCluster = function (data) {
        var small, large;
        if (this.plt.is('android')) {
            small = "./assets/markercluster/small.png";
            large = "./assets/markercluster/large.png";
        }
        else if (this.plt.is('ios')) {
            small = "www/assets/markercluster/small.png";
            large = "www/assets/markercluster/large.png";
        }
        var options = {
            markers: data,
            icons: [
                {
                    min: 3,
                    max: 9,
                    url: small,
                    size: {
                        height: 29,
                        width: 29
                    },
                    label: {
                        color: "white"
                    }
                },
                {
                    min: 10,
                    url: large,
                    size: {
                        height: 37,
                        width: 37
                    },
                    label: {
                        color: "white"
                    }
                }
            ],
            boundsDraw: false,
            maxZoomLevel: 15
        };
        var markerCluster = this.allData.map.addMarkerClusterSync(options);
        markerCluster.on(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function (params) {
            var marker = params[1];
            marker.setTitle(marker.get("name"));
            marker.setSnippet(marker.get("address"));
            marker.showInfoWindow();
        });
    };
    LivePage.prototype.navigateFromCurrentLoc = function () {
        window.open("https://www.google.com/maps/dir/?api=1&destination=" + this.recenterMeLat + "," + this.recenterMeLng + "&travelmode=driving", "_blank");
    };
    LivePage.prototype.calcRoute = function (start, end) {
        var that = this;
        that.allData.AIR_PORTS = [];
        var directionsService = new google.maps.DirectionsService();
        var request = {
            origin: start,
            destination: end,
            optimizeWaypoints: true,
            travelMode: google.maps.TravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var path = new google.maps.MVCArray();
                for (var i = 0, len = response.routes[0].overview_path.length; i < len; i++) {
                    path.push(response.routes[0].overview_path[i]);
                    that.allData.AIR_PORTS.push({
                        lat: path.g[i].lat(), lng: path.g[i].lng()
                    });
                    if (that.allData.AIR_PORTS.length > 1) {
                        that.allData.map.addPolyline({
                            'points': that.allData.AIR_PORTS,
                            'color': '#4aa9d5',
                            'width': 4,
                            'geodesic': true,
                        }).then(function (poly) {
                            that.polyLines.push(poly);
                            that.getTravelDetails(start, end);
                        });
                    }
                }
            }
        });
    };
    // service = new google.maps.DistanceMatrixService();
    LivePage.prototype.getTravelDetails = function (source, dest) {
        var _this = this;
        var that = this;
        that.service = new google.maps.DistanceMatrixService();
        this._id = setInterval(function () {
            if (localStorage.getItem("livepagetravelDetailsObject") != null) {
                if (that.expectation.distance == undefined && that.expectation.duration == undefined) {
                    that.expectation = JSON.parse(localStorage.getItem("livepagetravelDetailsObject"));
                }
                else {
                    clearInterval(_this._id);
                }
            }
        }, 3000);
        that.service.getDistanceMatrix({
            origins: [source],
            destinations: [dest],
            travelMode: 'DRIVING'
        }, that.callback);
        // that.apiCall.stopLoading();
    };
    LivePage.prototype.callback = function (response, status) {
        var travelDetailsObject;
        if (status == 'OK') {
            var origins = response.originAddresses;
            for (var i = 0; i < origins.length; i++) {
                var results = response.rows[i].elements;
                for (var j = 0; j < results.length; j++) {
                    var element = results[j];
                    var distance = element.distance.text;
                    var duration = element.duration.text;
                    travelDetailsObject = {
                        distance: distance,
                        duration: duration
                    };
                }
            }
            localStorage.setItem("livepagetravelDetailsObject", JSON.stringify(travelDetailsObject));
        }
    };
    LivePage.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
            // this.ngOnDestroy();
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].HYBRID);
            // this.someFunc();
        }
        else {
            if (maptype == 'TERRAIN') {
                // this.ngOnDestroy();
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
                // this.someFunc();
            }
            else {
                if (maptype == 'NORMAL') {
                    // this.ngOnDestroy();
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                    // this.someFunc();
                }
            }
        }
    };
    LivePage.prototype.settings = function () {
        var _this = this;
        var that = this;
        var profileModal = this.modalCtrl.create('DeviceSettingsPage', {
            param: that.deviceDeatils
        });
        profileModal.present();
        profileModal.onDidDismiss(function () {
            var paramData = _this.navParams.get("device");
            _this.temp(paramData);
        });
    };
    LivePage.prototype.addPOI = function () {
        var that = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__live_single_device_live_single_device__["b" /* PoiPage */], {
            param1: that.allData[that.impkey]
        });
        modal.onDidDismiss(function (data) {
            that.showaddpoibtn = false;
        });
        modal.present();
    };
    LivePage.prototype.onSelectMapOption = function (type) {
        var that = this;
        if (type == 'locateme') {
            that.allData.map.setCameraTarget(that.latlngCenter);
        }
        else {
            if (type == 'mapHideTraffic') {
                that.mapHideTraffic = !that.mapHideTraffic;
                if (that.mapHideTraffic) {
                    that.allData.map.setTrafficEnabled(true);
                }
                else {
                    that.allData.map.setTrafficEnabled(false);
                }
            }
            else {
                if (type == 'showGeofence') {
                    if (that.generalPolygon != undefined) {
                        that.generalPolygon.remove();
                        that.generalPolygon = undefined;
                    }
                    else {
                        that.callGeofence();
                    }
                }
            }
        }
    };
    LivePage.prototype.callGeofence = function () {
        var _this = this;
        this.geoShape = [];
        this.apiCall.startLoading().present();
        this.apiCall.getGeofenceCall(this.userdetails._id)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            if (data.length > 0) {
                _this.geoShape = data.map(function (d) {
                    return d.geofence.coordinates[0];
                });
                for (var g = 0; g < _this.geoShape.length; g++) {
                    for (var v = 0; v < _this.geoShape[g].length; v++) {
                        _this.geoShape[g][v] = _this.geoShape[g][v].reverse();
                    }
                }
                for (var t = 0; t < _this.geoShape.length; t++) {
                    _this.drawPolygon(_this.geoShape[t]);
                }
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    message: 'No gofence found..!!',
                    buttons: ['OK']
                });
                alert_1.present();
            }
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.drawPolygon = function (polyData) {
        var _this = this;
        var that = this;
        that.geodata = [];
        that.geodata = polyData.map(function (d) {
            return { lat: d[0], lng: d[1] };
        });
        var GORYOKAKU_POINTS = that.geodata;
        that.allData.map.addPolygon({
            'points': GORYOKAKU_POINTS,
            'strokeColor': '#AA00FF',
            'fillColor': '#00FFAA',
            'strokeWidth': 2
        }).then(function (polygon) {
            _this.generalPolygon = polygon;
        });
    };
    LivePage.prototype.gotoAll = function () {
        this.navCtrl.setRoot('LivePage');
        localStorage.setItem("CLICKED_ALL", "CLICKED_ALL");
    };
    LivePage.prototype.shareLive = function () {
        var _this = this;
        var data = {
            id: this.liveDataShare._id,
            imei: this.liveDataShare.Device_ID,
            sh: this.userdetails._id,
            ttl: 60 // set to 1 hour by default
        };
        this.apiCall.startLoading().present();
        this.apiCall.shareLivetrackCall(data)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.resToken = data.t;
            _this.liveShare();
        }, function (err) {
            _this.apiCall.stopLoading();
            console.log(err);
        });
    };
    LivePage.prototype.getPOIs = function () {
        var _this = this;
        this.allData._poiData = [];
        var _burl = this.apiCall.mainUrl + "poi/getPois?user=" + this.userdetails._id;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(_burl)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.allData._poiData = data;
            _this.plotPOI();
        });
    };
    LivePage.prototype.plotPOI = function () {
        var toast = this.toastCtrl.create({
            message: "Plotting POI's please wait...",
            position: 'middle'
        });
        var icurl;
        if (this.plt.is('android')) {
            icurl = './assets/imgs/poiFlag.png';
        }
        else if (this.plt.is('ios')) {
            icurl = 'www/assets/imgs/poiFlag.png';
        }
        for (var v = 0; v < this.allData._poiData.length; v++) {
            toast.present();
            if (this.allData._poiData[v].poi.location.coordinates !== undefined) {
                this.allData.map.addMarker({
                    title: this.allData._poiData[v].poi.poiname,
                    position: {
                        lat: this.allData._poiData[v].poi.location.coordinates[1],
                        lng: this.allData._poiData[v].poi.location.coordinates[0]
                    },
                    icon: {
                        url: icurl,
                        size: {
                            width: 20,
                            height: 20
                        }
                    },
                    animation: __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["c" /* GoogleMapsAnimation */].BOUNCE
                });
            }
        }
        toast.dismiss();
    };
    LivePage.prototype.liveShare = function () {
        var that = this;
        var link = this.apiCall.mainUrl + "share/liveShare?t=" + that.resToken;
        that.socialSharing.share(that.userdetails.fn + " " + that.userdetails.ln + " has shared " + that.liveDataShare.Device_Name + " live trip with you. Please follow below link to track", "OneQlik- Live Trip", "", link);
    };
    LivePage.prototype.zoomin = function () {
        var that = this;
        that.allData.map.animateCameraZoomIn();
    };
    LivePage.prototype.zoomout = function () {
        var that = this;
        that.allData.map.animateCameraZoomOut();
    };
    LivePage.prototype.getIconUrl = function (data) {
        var that = this;
        var iconUrl;
        if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
            if (that.plt.is('ios')) {
                iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
            }
            else if (that.plt.is('android')) {
                iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
            }
        }
        else {
            if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
                if (that.plt.is('ios')) {
                    iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
                else if (that.plt.is('android')) {
                    iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
            }
            else {
                var stricon;
                if (data.status.toLowerCase() === 'out of reach') {
                    stricon = "outofreach";
                    if (that.plt.is('ios')) {
                        iconUrl = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                    }
                    else if (that.plt.is('android')) {
                        iconUrl = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                    }
                }
                else {
                    if (that.plt.is('ios')) {
                        iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                    }
                    else if (that.plt.is('android')) {
                        iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                    }
                }
            }
        }
        return iconUrl;
    };
    LivePage.prototype.parseMillisecondsIntoReadableTime = function (milliseconds) {
        //Get hours from milliseconds
        var hours = milliseconds / (1000 * 60 * 60);
        var absoluteHours = Math.floor(hours);
        var h = absoluteHours > 9 ? absoluteHours : '0' + absoluteHours;
        //Get remainder from hours and convert to minutes
        var minutes = (hours - absoluteHours) * 60;
        var absoluteMinutes = Math.floor(minutes);
        var m = absoluteMinutes > 9 ? absoluteMinutes : '0' + absoluteMinutes;
        //Get remainder from minutes and convert to seconds
        var seconds = (minutes - absoluteMinutes) * 60;
        var absoluteSeconds = Math.floor(seconds);
        var s = absoluteSeconds > 9 ? absoluteSeconds : '0' + absoluteSeconds;
        // return h + ':' + m;
        return h + ':' + m + ':' + s;
    };
    LivePage.prototype.otherValues = function (data) {
        var that = this;
        that.vehicle_speed = data.last_speed;
        that.todays_odo = data.today_odo;
        that.total_odo = data.total_odo;
        if (that.userdetails.fuel_unit == 'LITRE') {
            that.fuel = data.currentFuel;
        }
        else if (that.userdetails.fuel_unit == 'PERCENTAGE') {
            that.fuel = data.fuel_percent;
        }
        else {
            that.fuel = data.currentFuel;
        }
        that.voltage = data.battery;
        if (data.last_location) {
            that.getAddress(data.last_location);
        }
        that.last_ping_on = data.last_ping_on;
        var tempvar = new Date(data.lastStoppedAt);
        if (data.lastStoppedAt != null) {
            var fd = tempvar.getTime();
            var td = new Date().getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            that.lastStoppedAt = rhours + ':' + rminutes;
        }
        else {
            that.lastStoppedAt = '00' + ':' + '00';
        }
        that.distFromLastStop = data.distFromLastStop;
        if (!isNaN(data.timeAtLastStop)) {
            that.timeAtLastStop = that.parseMillisecondsIntoReadableTime(data.timeAtLastStop);
        }
        else {
            that.timeAtLastStop = '00:00:00';
        }
        that.today_stopped = that.parseMillisecondsIntoReadableTime(data.today_stopped);
        that.today_running = that.parseMillisecondsIntoReadableTime(data.today_running);
        that.last_ACC = data.last_ACC;
        that.acModel = data.ac;
        that.currentFuel = data.currentFuel;
        that.power = data.power;
        that.gpsTracking = data.gpsTracking;
        that.tempreture = data.temp;
        that.door = data.door;
        that.recenterMeLat = data.last_location.lat;
        that.recenterMeLng = data.last_location.long;
    };
    LivePage.prototype.weDidntGetPing = function (data) {
        var that = this;
        if (data._id != undefined && data.last_location != undefined) {
            that.otherValues(data);
            if (data.last_location != undefined) {
                that.allData.map.addMarker({
                    title: data.Device_Name,
                    position: { lat: data.last_location.lat, lng: data.last_location.long },
                    icon: {
                        url: that.getIconUrl(data),
                        size: {
                            width: 20,
                            height: 40
                        }
                    },
                }).then(function (marker) {
                    if (that.selectedVehicle) {
                        that.temporaryMarker = marker;
                    }
                    else {
                        that.tempMarkArray.push(marker);
                    }
                    // that.allData[key].mark = marker;
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) {
                        that.liveVehicleName = data.Device_Name;
                        // that.drawerHidden = false;
                        that.showaddpoibtn = true;
                        that.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                        that.onClickShow = true;
                    });
                    that.getAddress(data.last_location);
                    that.socketInit(data);
                });
            }
        }
    };
    LivePage.prototype.socketInit = function (pdata, center) {
        if (center === void 0) { center = false; }
        var that = this;
        that.socketChnl.push(pdata.Device_ID);
        console.log("event: ", pdata.Device_ID + 'acc');
        that.socketConn._userACC.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    if (that.selectedVehicle !== undefined) {
                        that.reCenterMe();
                        var temp123_1;
                        if (that.temporaryMarker) {
                            that.temporaryMarker.remove();
                            temp123_1 = "cleared";
                        }
                        that.temporaryInterval = setInterval(function () {
                            if (that.temporaryMarker) {
                                that.temporaryMarker.remove();
                                clearInterval(that.temporaryInterval);
                            }
                            else {
                                if (temp123_1 === 'cleared') {
                                    if (that.temporaryInterval) {
                                        clearInterval(that.temporaryInterval);
                                    }
                                }
                            }
                        }, 10);
                    }
                    else {
                        if (that.tempMarkArray.length > 0) {
                            for (var t = 0; t < that.tempMarkArray.length; t++) {
                                that.tempMarkArray[t].remove();
                            }
                        }
                    }
                    if (data._id != undefined && data.last_location != undefined) {
                        var key_1 = data._id;
                        that.impkey = data._id;
                        that.deviceDeatils = data;
                        var ic = {};
                        // let ic = _.cloneDeep(that.icons[data.iconType]);
                        // if (!ic) {
                        //   return;
                        // }
                        // ic.path = null;
                        // ic.url = that.getIconUrl(data);
                        if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0)) {
                            if (that.plt.is('ios')) {
                                ic.url = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
                            }
                            else if (that.plt.is('android')) {
                                ic.url = './assets/imgs/vehicles/running' + data.iconType + '.png';
                            }
                        }
                        else {
                            if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || (data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0)) {
                                if (that.plt.is('ios')) {
                                    ic.url = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                                }
                                else if (that.plt.is('android')) {
                                    ic.url = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                                }
                            }
                            else {
                                var stricon = void 0;
                                if (data.status.toLowerCase() === 'out of reach') {
                                    stricon = "outofreach";
                                    if (that.plt.is('ios')) {
                                        ic.url = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                                    }
                                    else if (that.plt.is('android')) {
                                        ic.url = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                                    }
                                }
                                else {
                                    if (that.plt.is('ios')) {
                                        ic.url = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                                    }
                                    else if (that.plt.is('android')) {
                                        ic.url = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                                    }
                                }
                            }
                        }
                        that.otherValues(data);
                        if (data.last_location != undefined) {
                            that.getAddress(data.last_location);
                        }
                        var strStr = void 0;
                        if (that.allData[key_1]) {
                            strStr = ' Address: ' + that.address + '\n Last Updated: ' + __WEBPACK_IMPORTED_MODULE_11_moment__(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
                            // that.socketSwitch[key] = data;
                            if (that.allData[key_1].mark != undefined && that.allData[key_1].mark1 != undefined) {
                                that.allData[key_1].mark.on(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function () {
                                    setTimeout(function () {
                                        that.allData[key_1].mark.hideInfoWindow();
                                    }, 2000);
                                });
                                that.allData[key_1].mark.setSnippet(strStr);
                                that.allData[key_1].mark.setIcon(ic.url);
                                that.allData[key_1].mark.setPosition(that.allData[key_1].poly[1]);
                                var temp = __WEBPACK_IMPORTED_MODULE_4_lodash__["cloneDeep"](that.allData[key_1].poly[1]);
                                that.allData[key_1].poly[0] = __WEBPACK_IMPORTED_MODULE_4_lodash__["cloneDeep"](temp);
                                that.allData[key_1].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                if (that.displayNames) {
                                    that.allData[key_1].mark1.setPosition(that.allData[key_1].poly[1]);
                                    that.liveTrack(that.allData.map, that.allData[key_1].mark, that.allData[key_1].mark1, that.allData[key_1].poly, parseFloat(speed), 10, center, data._id, that);
                                }
                                else {
                                    that.liveTrack(that.allData.map, that.allData[key_1].mark, null, that.allData[key_1].poly, parseFloat(speed), 10, center, data._id, that);
                                }
                            }
                            else {
                                return;
                            }
                        }
                        else {
                            that.allData[key_1] = {};
                            // that.socketSwitch[key] = data;
                            that.allData[key_1].poly = [];
                            that.allData[key_1].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            if (data.last_location != undefined) {
                                strStr = ' Address: ' + that.address + '\n Last Updated: ' + __WEBPACK_IMPORTED_MODULE_11_moment__(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
                                if (that.displayNames) {
                                    that.showMergedMarkers(data, key_1, ic, center);
                                }
                                else {
                                    that.showSimplemarkers(data, key_1, ic, center);
                                }
                                ///////////////
                                if (that.selectedVehicle) {
                                    // that.addCircleWithAnimation(that.allData[data._id].poly[0].lat, that.allData[data._id].poly[0].lng);
                                }
                                //////////////
                            }
                        }
                    }
                })(d4);
        });
    };
    LivePage.prototype.liveTrack = function (map, mark, mark1, coords, speed, delay, center, id, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        if (center) {
            map.setCameraTarget(coords[0]);
        }
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = 0;
            var lng = 0;
            if (mark.getPosition().lat !== undefined || mark.getPosition().lng !== undefined) {
                lat = mark.getPosition().lat;
                lng = mark.getPosition().lng;
            }
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
                // mark.setIcon(icons);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    // if (that.selectedVehicle != undefined) {
                    //   map.setCameraTarget(new LatLng(lat, lng));
                    // }
                    that.latlngCenter = new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](lat, lng);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    if (that.displayNames) {
                        mark1.setPosition(new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    }
                    /////////
                    // that.circleObj.setCenter(new LatLng(lat, lng))
                    ////////
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    // if (that.selectedVehicle != undefined) {
                    //   map.setCameraTarget(dest);
                    // }
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    if (that.displayNames) {
                        mark1.setPosition(dest);
                    }
                    /////////
                    // that.circleObj.setCenter(new LatLng(dest.lat, dest.lng))
                    ////////
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LivePage.prototype.showSimplemarkers = function (data, key, ic, center) {
        var that = this;
        that.allData.map.addMarker({
            title: data.Device_Name,
            position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
            icon: {
                url: ic,
                size: {
                    height: 40,
                    width: 20
                }
            },
        }).then(function (marker) {
            that.allData.markersArray.push(marker);
            that.allData[key].mark = marker;
            marker.addEventListener(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                .subscribe(function (e) {
                if (that.selectedVehicle == undefined) {
                    that.getAddressTitle(marker);
                }
                else {
                    that.liveVehicleName = data.Device_Name;
                    that.showaddpoibtn = true;
                    that.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                    that.onClickShow = true;
                }
            });
            var speed = data.status == "RUNNING" ? data.last_speed : 0;
            that.liveTrack(that.allData.map, that.allData[key].mark, null, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
        });
    };
    LivePage.prototype.showMergedMarkers = function (data, key, ic, center) {
        var that = this;
        /////////////// merge images using canvas
        var c = document.createElement("canvas");
        c.width = 100;
        c.height = 60;
        var ctx = c.getContext("2d");
        ctx.fillStyle = "#1c2f66";
        ctx.fillRect(0, 0, 100, 20);
        // ctx.strokeRect(0, 0, 100, 70);
        ctx.font = "9pt sans-serif";
        ctx.fillStyle = "white";
        ctx.fillText(data.Device_Name, 12, 15);
        var img = c.toDataURL();
        that.allData.map.addMarker({
            position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
            icon: {
                url: img,
                size: {
                    height: 60,
                    width: 100
                }
            },
        }).then(function (marker1) {
            that.allData.markersArray.push(marker1);
            that.allData[key].mark1 = marker1;
            that.allData.map.addMarker({
                position: { lat: that.allData[key].poly[0].lat, lng: that.allData[key].poly[0].lng },
                icon: {
                    url: ic.url,
                    size: {
                        height: 40,
                        width: 20
                    }
                },
            }).then(function (marker) {
                that.allData.markersArray.push(marker);
                that.allData[key].mark = marker;
                marker.addEventListener(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                    .subscribe(function (e) {
                    if (that.selectedVehicle == undefined) {
                        that.getAddressTitle(marker);
                    }
                    else {
                        that.liveVehicleName = data.Device_Name;
                        that.showaddpoibtn = true;
                        that.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                        that.onClickShow = true;
                    }
                });
                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                that.liveTrack(that.allData.map, that.allData[key].mark, that.allData[key].mark1, that.allData[key].poly, parseFloat(speed), 10, center, data._id, that);
            });
        });
    };
    LivePage.prototype.addCircleWithAnimation = function (lat, lng) {
        var that = this;
        var _radius = 500;
        var rMin = _radius * 1 / 9;
        var rMax = _radius;
        var direction = 1;
        var GOOGLE = {
            "lat": lat,
            "lng": lng
        };
        var circleOption = {
            'center': GOOGLE,
            'radius': 500,
            'fillColor': 'rgb(216, 6, 34)',
            'fillOpacity': 0.6,
            'strokeColor': '#950417',
            'strokeOpacity': 1,
            'strokeWidth': 0.5
        };
        // Add circle
        var circle = that.allData.map.addCircleSync(circleOption);
        that.circleObj = circle;
        that.circleTimer = setInterval(function () {
            var radius = circle.getRadius();
            if ((radius > rMax) || (radius < rMin)) {
                direction *= -1;
            }
            var _par = (radius / _radius) - 0.1;
            var opacity = 0.4 * _par;
            var colorString = that.RGBAToHexA(216, 6, 34, opacity);
            circle.setFillColor(colorString);
            circle.setRadius(radius + direction * 10);
        }, 30);
    };
    LivePage.prototype.RGBAToHexA = function (r, g, b, a) {
        r = r.toString(16);
        g = g.toString(16);
        b = b.toString(16);
        a = Math.round(a * 255).toString(16);
        if (r.length == 1)
            r = "0" + r;
        if (g.length == 1)
            g = "0" + g;
        if (b.length == 1)
            b = "0" + b;
        if (a.length == 1)
            a = "0" + a;
        return "#" + r + g + b + a;
    };
    LivePage.prototype.getAddress = function (coordinates) {
        var that = this;
        if (!coordinates) {
            that.address = 'N/A';
            return;
        }
        this.geocoderApi.reverseGeocode(coordinates.lat, coordinates.long)
            .then(function (res) {
            var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
            // that.saveAddressToServer(str, coordinates.lat, coordinates.long);
            that.address = str;
        });
    };
    LivePage.prototype.showNearby = function (key) {
        var _this = this;
        // for police stations
        var url, icurl;
        if (key === 'police') {
            if (this.policeButtonColor == '#012B72') {
                this.policeButtonColor = '#f4f4f4';
                this.policeColor = '#012B72';
            }
            else {
                if (this.policeButtonColor == '#f4f4f4') {
                    this.policeButtonColor = '#012B72';
                    this.policeColor = '#fff';
                }
            }
            if (this.plt.is('android')) {
                icurl = './assets/imgs/police-station.png';
            }
            else if (this.plt.is('ios')) {
                icurl = 'www/assets/imgs/police-station.png';
            }
            url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=police&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
        }
        else if (key === 'petrol') {
            if (this.petrolButtonColor == '#012B72') {
                this.petrolButtonColor = '#f4f4f4';
                this.petrolColor = '#012B72';
            }
            else {
                if (this.petrolButtonColor == '#f4f4f4') {
                    this.petrolButtonColor = '#012B72';
                    this.petrolColor = '#fff';
                }
            }
            if (this.plt.is('android')) {
                icurl = './assets/imgs/gas-pump.png';
            }
            else if (this.plt.is('ios')) {
                icurl = 'www/assets/imgs/gas-pump.png';
            }
            url = this.apiCall.mainUrl + "googleAddress/getNearBY?lat=" + this.recenterMeLat + "&long=" + this.recenterMeLng + "&radius=1000&type=gas_station&key=AIzaSyCNT3eO1wPQHUhY_cmQ9N_9BkLzJ_GB9j8";
        }
        this.apiCall.getSOSReportAPI(url).subscribe(function (resp) {
            if (resp.status === 'OK') {
                var mapData = resp.results.map(function (d) {
                    return { lat: d.geometry.location.lat, lng: d.geometry.location.lng };
                });
                var bounds = new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["g" /* LatLngBounds */](mapData);
                for (var t = 0; t < resp.results.length; t++) {
                    _this.allData.map.addMarker({
                        title: resp.results[t].name,
                        position: {
                            lat: resp.results[t].geometry.location.lat,
                            lng: resp.results[t].geometry.location.lng
                        },
                        icon: {
                            url: icurl,
                            size: {
                                height: 35,
                                width: 35
                            }
                        }
                    }).then(function (mark) {
                    });
                }
                _this.allData.map.moveCamera({
                    target: bounds
                });
                _this.allData.map.setPadding(20, 20, 20, 20);
                _this.zoomLevel = 10;
            }
            else if (resp.status === 'ZERO_RESULTS') {
                if (key === 'police') {
                    _this.showToastMsg('No nearby police stations found.');
                }
                else if (key === 'petrol') {
                    _this.showToastMsg('No nearby petrol pump found.');
                }
            }
            else if (resp.status === 'OVER_QUERY_LIMIT') {
                _this.showToastMsg('Query limit exeed. Please try after some time.');
            }
            else if (resp.status === 'REQUEST_DENIED') {
                _this.showToastMsg('Please check your API key.');
            }
            else if (resp.status === 'INVALID_REQUEST') {
                _this.showToastMsg('Invalid request. Please try again.');
            }
            else if (resp.status === 'UNKNOWN_ERROR') {
                _this.showToastMsg('An unknown error occured. Please try again.');
            }
        });
    };
    LivePage.prototype.showToastMsg = function (msg) {
        this.toastCtrl.create({
            message: msg,
            duration: 2000,
            position: 'bottom'
        }).present();
    };
    LivePage.prototype.reCenterMe = function () {
        this.allData.map.animateCamera({
            target: { lat: this.recenterMeLat, lng: this.recenterMeLng },
            tilt: 30,
            duration: 1800
        }).then(function () {
        });
    };
    LivePage.prototype.getAddressTitle = function (marker) {
        var payload = {
            "lat": marker.getPosition().lat,
            "long": marker.getPosition().lng,
            "api_id": "1"
        };
        var addr;
        this.apiCall.getAddressApi(payload)
            .subscribe(function (data) {
            if (data.results[2] != undefined || data.results[2] != null) {
                addr = data.results[2].formatted_address;
            }
            else {
                addr = 'N/A';
            }
            marker.setSnippet(addr);
        });
    };
    LivePage.prototype.info = function (mark, cb) {
        mark.addListener('click', cb);
    };
    LivePage.prototype.setDocHeight = function () {
        var that = this;
        that.distanceTop = 200;
        that.drawerState = __WEBPACK_IMPORTED_MODULE_7_ion_bottom_drawer__["a" /* DrawerState */].Top;
    };
    LivePage.prototype.temp = function (data) {
        var _this = this;
        localStorage.setItem("navigationFrom", "MultiLiveScreen");
        if (data.status === 'Expired') {
            var profileModal = this.modalCtrl.create('ExpiredPage', {
                param: data
            });
            profileModal.present();
            profileModal.onDidDismiss(function () {
                _this.selectedVehicle = undefined;
            });
        }
        else {
            this.navCtrl.push('LiveSingleDevice', { device: data }, {
                animate: true, animation: 'transition-ios'
            });
        }
    };
    LivePage.prototype.socketInitForSingleVehicle = function (pdata, center) {
        if (center === void 0) { center = false; }
        this._io.emit('acc', pdata.Device_ID);
        this.socketChnl.push(pdata.Device_ID + 'acc');
        var that = this;
        this._io.on(pdata.Device_ID + 'acc', function (d1, d2, d3, d4, d5) {
            if (d4 != undefined)
                (function (data) {
                    if (data == undefined) {
                        return;
                    }
                    localStorage.setItem("gotPing", "true");
                    that.reCenterMe();
                    localStorage.setItem("pdata", JSON.stringify(data));
                    if (data._id != undefined && data.last_location != undefined) {
                        var key_2 = data._id;
                        that.impkey = data._id;
                        that.deviceDeatils = data;
                        that.otherValues(data);
                        var strStr = void 0;
                        if (that.allData[key_2]) {
                            strStr = ' Address: ' + that.address + '\n Last Updated: ' + __WEBPACK_IMPORTED_MODULE_11_moment__(new Date(data.last_ping_on)).format('LLLL') + '\n Speed: ' + data.last_speed;
                            // that.socketSwitch[key] = data;
                            if (that.allData[key_2].mark !== undefined) {
                                that.allData[key_2].mark.on(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function () {
                                    setTimeout(function () {
                                        that.allData[key_2].mark.hideInfoWindow();
                                    }, 2000);
                                });
                                that.allData[key_2].mark.setSnippet(strStr);
                                that.allData[key_2].mark.setIcon(that.getIconUrl(data));
                                that.allData[key_2].mark.setPosition(that.allData[key_2].poly[1]);
                                var temp = __WEBPACK_IMPORTED_MODULE_4_lodash__["cloneDeep"](that.allData[key_2].poly[1]);
                                that.allData[key_2].poly[0] = __WEBPACK_IMPORTED_MODULE_4_lodash__["cloneDeep"](temp);
                                that.allData[key_2].poly[1] = { lat: data.last_location.lat, lng: data.last_location.long };
                                var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                that.getAddress(data.last_location);
                                that.liveTrackForSingleVehicle(that.allData.map, that.allData[key_2].mark, that.allData[key_2].poly, parseFloat(speed), 10, center, data._id, that);
                            }
                            else {
                                return;
                            }
                        }
                        else {
                            that.allData[key_2] = {};
                            // that.socketSwitch[key] = data;
                            that.allData[key_2].poly = [];
                            if (data.sec_last_location.lat !== null && data.sec_last_location.long !== null) {
                                that.allData[key_2].poly.push({ lat: data.sec_last_location.lat, lng: data.sec_last_location.long });
                            }
                            else {
                                that.allData[key_2].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            }
                            that.allData[key_2].poly.push({ lat: data.last_location.lat, lng: data.last_location.long });
                            if (data.last_location != undefined) {
                                if (that.temporaryMarker123 !== undefined) {
                                    that.temporaryMarker123.setTitle(data.Device_Name);
                                    that.temporaryMarker123.setPosition(that.allData[key_2].poly[0]);
                                    that.allData[key_2].mark = that.temporaryMarker123;
                                    that.allData[key_2].mark.addEventListener(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                        .subscribe(function (e) {
                                        that.liveVehicleName = data.Device_Name;
                                        that.showaddpoibtn = true;
                                        that.getAddressTitle(that.allData[key_2].mark);
                                    });
                                    that.getAddress(data.last_location);
                                    var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                    that.liveTrackForSingleVehicle(that.allData.map, that.allData[key_2].mark, that.allData[key_2].poly, parseFloat(speed), 10, center, data._id, that);
                                }
                                else {
                                    that.allData.map.addMarker({
                                        title: data.Device_Name,
                                        position: { lat: that.allData[key_2].poly[0].lat, lng: that.allData[key_2].poly[0].lng },
                                        icon: {
                                            url: that.getIconUrl(data),
                                            size: {
                                                height: 40,
                                                width: 20
                                            }
                                        },
                                    }).then(function (marker) {
                                        that.allData[key_2].mark = marker;
                                        marker.addEventListener(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                                            .subscribe(function (e) {
                                            that.liveVehicleName = data.Device_Name;
                                            that.showaddpoibtn = true;
                                            that.getAddressTitle(marker);
                                        });
                                        that.getAddress(data.last_location);
                                        var speed = data.status == "RUNNING" ? data.last_speed : 0;
                                        that.liveTrackForSingleVehicle(that.allData.map, that.allData[key_2].mark, that.allData[key_2].poly, parseFloat(speed), 10, center, data._id, that);
                                    });
                                }
                            }
                        }
                    }
                })(d4);
        });
    };
    LivePage.prototype.liveTrackForSingleVehicle = function (map, mark, coords, speed, delay, center, id, that) {
        var target = 0;
        clearTimeout(that.ongoingGoToPoint[id]);
        clearTimeout(that.ongoingMoveMarker[id]);
        function _goToPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000; // in meters
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](coords[target].lat, coords[target].lng);
            var distance = __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); // in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target].lat - lat) / numStep;
            var deltaLng = (coords[target].lng - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(lat, lng, map);
                    }
                    else {
                        if (that.allData.polylineID) {
                            that.tempArray = [];
                            that.allData.polylineID.remove();
                        }
                    }
                    that.latlngCenter = new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](lat, lng);
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    that.ongoingMoveMarker[id] = setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    head = head < 0 ? (360 + head) : head;
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    if (that.mapTrail) {
                        that.showTrail(dest.lat, dest.lng, map);
                    }
                    else {
                        if (that.allData.polylineID) {
                            that.tempArray = [];
                            that.allData.polylineID.remove();
                        }
                    }
                    that.latlngCenter = dest;
                    mark.setPosition(dest);
                    target++;
                    that.ongoingGoToPoint[id] = setTimeout(_goToPoint, delay);
                }
            }
            _moveMarker();
        }
        _goToPoint();
    };
    LivePage.prototype.weDidntGetPingForSingleVehicle = function (data) {
        var that = this;
        if (data._id != undefined && data.last_location != undefined) {
            that.otherValues(data);
            if (data.last_location != undefined) {
                that.allData.map.addMarker({
                    title: data.Device_Name,
                    position: { lat: data.last_location.lat, lng: data.last_location.long },
                    icon: {
                        url: that.getIconUrl(data),
                        size: {
                            height: 40,
                            width: 20
                        }
                    },
                }).then(function (marker) {
                    that.temporaryMarker123 = marker;
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_5__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) {
                        that.liveVehicleName = data.Device_Name;
                        that.showaddpoibtn = true;
                        that.getAddressTitle(marker);
                    });
                    that.getAddress(data.last_location);
                    that.socketInitForSingleVehicle(data);
                });
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], LivePage.prototype, "navBar", void 0);
    LivePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-live',template:/*ion-inline-start:"/Users/apple/Desktop/screenshots/trackzap-ionic/src/pages/live/live.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button *ngIf="showMenuBtn" ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-buttons start *ngIf="shwBckBtn">\n\n      <!-- <ion-buttons start> -->\n\n      <button ion-button small (click)="goBack()">\n\n        <ion-icon name="arrow-back"></ion-icon>{{ "Back" | translate }}\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>{{ "LIVE TRACKING" | translate }}\n\n      <!-- <span *ngIf="titleText">{{ "For" | translate }} {{ titleText }}</span> -->\n\n    </ion-title>\n\n    <ion-buttons end *ngIf="showBtn">\n\n      <button ion-button (click)="ionViewDidEnter()">\n\n        {{ "All Vehicles" | translate }}\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n  <ion-item>\n\n    <ion-label>{{ SelectVehicle }}</ion-label>\n\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="temp(selectedVehicle)">\n\n    </select-searchable>\n\n  </ion-item>\n\n</ion-header>\n\n\n\n<ion-content style="background-color: #f2f1f0" *ngIf=\'!hideMe\'>\n\n \n\n  <div id="map_canvas">\n\n\n\n    <ion-fab top left>\n\n      <button ion-fab color="light" mini>\n\n        <ion-icon color="gpsc" name="map"></ion-icon>\n\n      </button>\n\n      <ion-fab-list side="bottom">\n\n        <button ion-fab (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n\n          S\n\n        </button>\n\n        <button ion-fab (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n\n          T\n\n        </button>\n\n        <button ion-fab (click)="onClickMap(\'NORMAL\')" color="gpsc">\n\n          N\n\n        </button>\n\n      </ion-fab-list>\n\n    </ion-fab>\n\n\n\n    <ion-fab top right>\n\n      <button color="gpsc" mini ion-fab (click)="onSelectMapOption(\'mapHideTraffic\')">\n\n        <img src="assets/icon/trafficON.png" *ngIf="mapHideTraffic" />\n\n        <img src="assets/icon/trafficOFF.png" *ngIf="!mapHideTraffic" />\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 20%">\n\n      <button ion-fab mini (click)="onSelectMapOption(\'showGeofence\')" color="gpsc">\n\n        <img src="assets/imgs/geo.png" />\n\n      </button>\n\n    </ion-fab>\n\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 33%">\n\n      <button ion-fab mini (click)="getPOIs()" color="gpsc">\n\n        <ion-icon name="pin"></ion-icon>\n\n      </button>\n\n    </ion-fab>\n\n\n\n  </div>\n\n</ion-content>\n\n<!-- <page-live [deviceData]="selectedVehicle" *ngIf="selectedVehicle != undefined"></page-live> -->\n\n<div *ngIf="onClickShow" class="divPlan">\n\n  <ion-bottom-drawer [(state)]="drawerState" [dockedHeight]="dockedHeight" [shouldBounce]="shouldBounce"\n\n    [distanceTop]="distanceTop" [minimumHeight]="minimumHeight" (click)="setDocHeight()">\n\n    <div class="drawer-content">\n\n      <p padding-left style="font-size: 13px; padding-bottom: 3px; color:cornflowerblue;">\n\n        {{ "Last Updated On" | translate }} &mdash;\n\n        {{ last_ping_on | date: "medium" }}\n\n      </p>\n\n      <p padding-left style="font-size: 13px" *ngIf="!address">N/A</p>\n\n      <p padding-left style="font-size: 13px" *ngIf="address">{{ address }}</p>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-1 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ign_red.png" *ngIf="last_ACC == \'0\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ign_green.png" *ngIf="last_ACC == \'1\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ign_idle.png" *ngIf="last_ACC == null" width="20" height="20" />\n\n          <p>{{ "IGN" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-1 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/ac_na.png" *ngIf="acModel == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ac_present.png" *ngIf="acModel == \'1\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/ac_empty.png" *ngIf="acModel == \'0\'" width="20" height="20" />\n\n          <p>{{ "AC" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-1 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/fuel_available.png" *ngIf="currentFuel != null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/fuel_empty.png" *ngIf="currentFuel == null" width="20" height="20" />\n\n          <p>{{ "FUEL" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-1 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/power_na.png" *ngIf="power == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/pwer_disconnected.png" *ngIf="power == \'0\'" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/power_connected.png" *ngIf="power == \'1\'" width="20" height="20" />\n\n          <p>{{ "POWER" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-1 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/gps_na.png" *ngIf="gpsTracking == null" width="30" height="20" />\n\n          <img src="assets/imgs/statusIcons/gps_notPresent.png" *ngIf="gpsTracking == \'0\'" width="30" height="20" />\n\n          <img src="assets/imgs/statusIcons/gps_present.png" *ngIf="gpsTracking == \'1\'" width="30" height="20" />\n\n          <p>{{ "GPS" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-1 style="text-align:center">\n\n          <ion-icon name="thermometer" *ngIf="tempreture == null" style="color: gray; font-size: 1.6em;"></ion-icon>\n\n          <ion-icon name="thermometer" *ngIf="tempreture == \'0\'" color="danger" style="font-size: 1.6em;"></ion-icon>\n\n          <ion-icon name="thermometer" *ngIf="tempreture == \'1\'" color="secondary" style="font-size: 1.6em;"></ion-icon>\n\n          <p>{{ "Temp" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-1 style="text-align:center">\n\n          <img src="assets/imgs/statusIcons/car_no_data.png" *ngIf="door == null" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/car_door_open.png" *ngIf="door == 0" width="20" height="20" />\n\n          <img src="assets/imgs/statusIcons/car_door_close.png" *ngIf="door == 1" width="20" height="20" />\n\n          <p>{{ "Door" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!total_odo">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="total_odo">\n\n            {{ total_odo | number: "1.0-2" }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Odometer" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!vehicle_speed">\n\n            0 {{ "Km/hr" | translate }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="vehicle_speed">\n\n            {{ vehicle_speed }} {{ "Km/hr" | translate }}\n\n          </p>\n\n          <p style="font-size: 13px">\n\n            {{ "Speed" | translate }}\n\n          </p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="fuel">\n\n            {{ fuel }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!fuel">N/A</p>\n\n          <p style="font-size: 13px">{{ "Fuel" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="voltage">\n\n            {{ voltage | number: "1.0-2" }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!voltage">N/A</p>\n\n          <p style="font-size: 13px">{{ "Voltage" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="tempreture">\n\n            {{ tempreture | number: "1.0-2" }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold; margin: 0px; padding: 0px;" *ngIf="!tempreture">N/A</p>\n\n          <p style="font-size: 13px">{{ "Temp" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!distFromLastStop">\n\n            0{{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="distFromLastStop">\n\n            {{ distFromLastStop | number: "1.0-2" }}{{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "From Last Stop" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/milestone-png-1.png" width="30" height="35" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!todays_odo">\n\n            0{{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="todays_odo">\n\n            {{ todays_odo | number: "1.0-2" }}{{ "Kms" | translate }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!timeAtLastStop">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="timeAtLastStop">\n\n            {{ timeAtLastStop }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "At Last Stop" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/parkinglogo.png" width="30" height="30" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!today_stopped">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="today_stopped">\n\n            {{ today_stopped }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n      <hr />\n\n      <ion-row>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!lastStoppedAt">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="lastStoppedAt">\n\n            {{ lastStoppedAt }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "From Last Stop" | translate }}</p>\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <img src="assets/imgs/2530791.png" width="30" height="35" style="margin-top: 9%" />\n\n        </ion-col>\n\n        <ion-col width-33 style="text-align:center">\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="!today_running">\n\n            N/A\n\n          </p>\n\n          <p style="font-size: 14px; font-weight: bold" *ngIf="today_running">\n\n            {{ today_running }}\n\n          </p>\n\n          <p style="font-size: 13px">{{ "Total" | translate }}</p>\n\n        </ion-col>\n\n      </ion-row>\n\n    </div>\n\n  </ion-bottom-drawer>\n\n</div>'/*ion-inline-end:"/Users/apple/Desktop/screenshots/trackzap-ionic/src/pages/live/live.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_6__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_10__providers_geocoder_geocoder__["a" /* GeocoderProvider */],
            __WEBPACK_IMPORTED_MODULE_12__providers_socket_connection_socket_connection__["a" /* SocketConnectionProvider */]])
    ], LivePage);
    return LivePage;
}());

//# sourceMappingURL=live.js.map

/***/ })

});
//# sourceMappingURL=42.js.map