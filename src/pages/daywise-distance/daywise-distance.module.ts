import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DaywiseDistancePage } from './daywise-distance';

@NgModule({
  declarations: [
    DaywiseDistancePage
  ],
  imports: [
    IonicPageModule.forChild(DaywiseDistancePage),
  ],
  exports: [
  ]
})
export class DaywiseDistancePageModule {}
