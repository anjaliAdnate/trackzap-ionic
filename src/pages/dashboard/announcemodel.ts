import { Component } from "@angular/core";
import { ViewController, NavParams } from "ionic-angular";
import { ApiServiceProvider } from "../../providers/api-service/api-service";
import * as moment from 'moment';


@Component({
  template: `

<ion-list>
  <div class = "mainDiv">
          <!-- <ion-buttons end>
            <button ion-button icon-only (click)="dismiss()">
                <ion-icon name="close-circle"></ion-icon>
            </button>
          </ion-buttons> -->
          <!-- <div style="height: 50%; width:50%; border: 1px soild;">
kj
          </div> -->
      <div class="secondDiv" style="
          background: #ffffff;
    /* height: 26px; */
    /* width: 1100%; */
    /* width: max-content;
    margin-left: -6%; */
    ">
          <h3 style="text-align: center">Announcement</h3>
      <!-- <span style="text-align: center">Upcoming Expired Devices</span> -->
      <p style="text-align: center;
    font-family: fangsong;
    font-size: 0.85 em;
    font-size: 20px;">{{ this.msgg }}</p>

    <p (click)="dismiss()" style="text-align: center; color: red"> Close </p>
      <!-- <ion-row *ngFor="let d of finalData">
        <ion-col style="font-size: 0.85em;" >
          {{d.device_name}}
        </ion-col>
        <ion-col style="font-size: 0.85em">
        {{d.expDate}}
        </ion-col>
       </ion-row> -->


      </div>
  </div>
</ion-list>
  `,

styles: [`

.mainDiv {
    padding: 50px;
    height: 100%;
    margin-top: 50%
    /* background-color: rgba(0, 0, 0, 0.23); */
}

.secondDiv {
    padding-top: 20px;
    /* border-radius: 18px;
    border: 2px solid black; */
    background: white;
    /* height: 500px !important; */
    overflow: scroll;
    background: url("../assets/imgs/tendencias-en-innovacion-tecnologica-y-proptechs-inmobiliarias-533x337xsmart-cities-opt.jpg.pagespeed.ic_.zsvuwxwqfv-2018-03-02_12-58-50_266848.jpg")
}

     /* .scroll-content {
       left: 0;
     right: 0;
     top: 0;
      bottom: 0;
      position: absolute;
      z-index: 1;
      display: block;
      overflow-x: hidden;
       overflow-y: hidden;
      -webkit-overflow-scrolling: touch;
      will-change: scroll-position;
       contain: size style layout;
   } */

`]
})

export class AnnounceModelPage {
  min_time: any = "10";
  page: number = 0;
  limit: number = 8;
  stausdevice: string;
  datetimeStart: string;
  datetime: string;
  allDevices: any = []
  exp_devices: any = [];
  final : any= [];
  start: any;
  end: any;
  islogin: any;
  checkIfStat: string = "ALL";
  obj: any = {};
  finalData = [];
  msgg: any;

  constructor(
    navParams: NavParams,
    private viewCtrl: ViewController,
    public apiCall: ApiServiceProvider
  ) {
    console.log("final sub", this.finalData)
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    if (navParams.get("label") && navParams.get("value")) {
      this.stausdevice = localStorage.getItem('status');
      this.checkIfStat = this.stausdevice;
    } else {
      this.stausdevice = undefined;
    }
    console.log("navparam test: " +navParams.get("params"));
    this.obj = navParams.get("params");
    this.datetime = moment({ hours: 0 }).format('LLLL');
    console.log("current date =>", this.datetime);
    this.start = this.datetime;
    this.datetimeStart = moment({ hours: 0 }).add(6, 'days').format('LLLL');
    console.log("upcoming week date for expired device =>", this.datetimeStart);
    this.end = this.datetimeStart
    console.log("start date", this.start);

    console.log("end date", this.end);
    //this.popUpForExpiredDevices();
    this.presentPopover();
    //this.setCookie('displayedPopupNewsletter', 'yes', 1);
  }

  setCookie( c_name, value, exdays ) {
    var c_value = escape(value);
    if (exdays) {
      var exdate = new Date();
      exdate.setDate( exdate.getDate() + exdays );
      c_value += '; expires=' + exdate.toUTCString();
    }
    document.cookie = c_name + '=' + c_value;
  }

  presentPopover() {
    var b_url = this.apiCall.mainUrl + "users/get_user_setting";
    var Var = { uid: this.islogin._id };
    this.apiCall.urlpasseswithdata(b_url, Var)
      .subscribe(resp => {
        console.log("check lang key: ", resp)
        this.msgg = resp.announcement;
        console.log("announcement", this.msgg);
      },
        err => {
          console.log(err);
        });
  }


  popUpForExpiredDevices() {
    var baseURLp;
    var vehName = [];
    var final = [];
    if (this.stausdevice) {
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice ;
    }
    else {
      baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    }

    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    //this.allDevices = []
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
    .subscribe(data => {
    console.log("vechicle list for expired devices =>", data);
    this.allDevices = data.devices
    console.log("data", this.allDevices)


    final  = [], vehName = [];
    for(var t = 0; t < this.allDevices.length; t++) {
      debugger;
      let vehExpDate = moment(this.allDevices[t].expiration_date).format('LLLL');
      //console.log("expf", vehExpDate)
      var sd = new Date(this.start);
      var td = new Date(this.end);
      var expDate = new Date(vehExpDate);
      // (vehExpDate <= this.end && vehExpDate >= this.start
      if ((expDate.getTime()>=sd.getTime())&&(expDate.getTime()<=td.getTime())) {
        // count ++
        // vehName.push(this.allDevices[t].Device_Name);
        // final.push(vehExpDate);

        var expDevices ={
          device_name : this.allDevices[t].Device_Name,
          expDate :vehExpDate
        }
        this.finalData.push(expDevices);
      }

    }
    console.log("exp_date", final);
    //console.log("exp_date2",final);
    console.log("dname", vehName);
    },
      err => {
        console.log("error=> ", err);
      });


  }

  // doInfinite(infiniteScroll) {
  //   var baseURLp;
  //   var vehName = [];
  //   var final = [];
  //   if (this.stausdevice) {
  //     baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email + '&statuss=' + this.stausdevice ;
  //   }
  //   else {
  //     baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
  //   }

  //   if (this.islogin.isSuperAdmin == true) {
  //     baseURLp += '&supAdmin=' + this.islogin._id;
  //   } else {
  //     if (this.islogin.isDealer == true) {
  //       baseURLp += '&dealer=' + this.islogin._id;
  //     }
  //   }
  //   //this.allDevices = []
  //   this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
  //   .subscribe(data => {
  //   console.log("vechicle list for expired devices =>", data);
  //   this.allDevices = data.devices
  //   console.log("data", this.allDevices)


  //   final  = [], vehName = [];
  //   for(var t = 0; t < this.allDevices.length; t++) {
  //     debugger;
  //     let vehExpDate = moment(this.allDevices[t].expiration_date).format('LLLL');
  //     //console.log("expf", vehExpDate)
  //     var sd = new Date(this.start);
  //     var td = new Date(this.end);
  //     var expDate = new Date(vehExpDate);
  //     // (vehExpDate <= this.end && vehExpDate >= this.start
  //     if ((expDate.getTime()>=sd.getTime())&&(expDate.getTime()<=td.getTime())) {
  //       // count ++
  //       // vehName.push(this.allDevices[t].Device_Name);
  //       // final.push(vehExpDate);

  //       var expDevices ={
  //         device_name : this.allDevices[t].Device_Name,
  //         expDate :vehExpDate
  //       }
  //       this.finalData.push(expDevices);
  //     }

  //   }
  //   infiniteScroll.complete();
  //   })
  // }
    // let that = this;
    // that.page = that.page + 1;
    // setTimeout(() => {
    //   var baseURLp;
    //   if (that.stausdevice) {
    //     baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&statuss=' + that.stausdevice + '&skip=' + that.page + '&limit=' + that.limit;
    //   }
    //   else {
    //     baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUser?id=' + that.islogin._id + '&email=' + that.islogin.email + '&skip=' + that.page + '&limit=' + that.limit;
    //   }

    //   if (this.islogin.isSuperAdmin == true) {
    //     baseURLp += '&supAdmin=' + this.islogin._id;
    //   } else {
    //     if (this.islogin.isDealer == true) {
    //       baseURLp += '&dealer=' + this.islogin._id;
    //     }
    //   }
    //   that.ndata = [];
    //   that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
    //     .subscribe(
    //       res => {
    //         that.ndata = res.devices;
    //         for (let i = 0; i < that.ndata.length; i++) {
    //           that.allDevices.push(that.ndata[i]);
    //         }
    //         that.allDevicesSearch = that.allDevices;
    //         infiniteScroll.complete();
    //       },
    //       error => {
    //         console.log(error);
    //       });

    // }, 100);



  dismiss() {
    console.log("Inside the function");
    this.viewCtrl.dismiss();
  }
}
