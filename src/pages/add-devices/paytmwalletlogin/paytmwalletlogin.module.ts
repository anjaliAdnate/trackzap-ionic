import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaytmwalletloginPage } from './paytmwalletlogin';
//import {PaymentHistoryPage} from './paytmwalletlogin';
//import {SuccessDetailPage} from './paytmwalletlogin';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PaytmwalletloginPage,
  ],
  imports: [
    IonicPageModule.forChild(PaytmwalletloginPage),
    TranslateModule.forChild()
  ],
})
export class PaytmwalletloginPageModule {}
