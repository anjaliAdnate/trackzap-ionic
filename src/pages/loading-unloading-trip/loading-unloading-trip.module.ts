import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoadingUnloadingTripPage } from './loading-unloading-trip';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    LoadingUnloadingTripPage,
  ],
  imports: [
    IonicPageModule.forChild(LoadingUnloadingTripPage),
    TranslateModule.forChild()
  ],
})
export class LoadingUnloadingTripPageModule {}
