import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoutePage } from './route';
import { TranslateModule } from '@ngx-translate/core';
// import { CreateRoutePage } from '../create-route/create-route';

@NgModule({
  declarations: [
    RoutePage,
    // CreateRoutePage
  ],
  imports: [
    IonicPageModule.forChild(RoutePage),
    TranslateModule.forChild()
  ],
  exports: [
    // CreateRoutePage
  ]
})
export class RoutePageModule {}
